"""
Filter: The filter() function extracts elements from an iterable (list, tuple etc.) for which a function returns True

Map: The map() function returns a map object(which is an iterator) of the results after applying the given function to each item of a given iterable (list, tuple etc.)

Reduce: The reduce(fun,seq) function is used to apply a particular function passed in its argument to all of the list elements mentioned in the sequence passed along.This function is defined in “functools” module.
"""

from  functools import reduce

def craft_simple_list(n):
    """Creating a simple list in a range from 1 to a given number"""

    return [k for k in range(1, n + 1)]


def filter_even_numbers(simple_list):
    """Just returning the even number of a given list using FILTER"""

    return list(filter(lambda x : x %2 == 0, simple_list))

def squaring_numbers_list(simple_list):
    """Returning a list with the square numbers of the original list"""
    return list(map(lambda x : x**2, simple_list))

def factorial_numbers_list(simple_list):
    return reduce(lambda x, y : x * y, simple_list)

"""
Handling errors

structure of assertion:

assertion <condition>, "message if the condition is not accomplish"
"""

def reversing_string(string):
    assert len(string) > 0, "Empty string are not allowed"
    return string[::-1]

def check_for_numbers(string):
    # Is numeric is a special method of string objects
    assert string.isnumeric(), "Only numeric characters are allowed"

if __name__ == "__main__":
    #a_list = craft_simple_list(100)
    #print(a_list)
    #print()
    # print(filter_even_numbers(a_list))
    # print(squaring_numbers_list(a_list))
    # print(factorial_numbers_list(a_list))
    # reversing_string("") # This will raise the assertion
    check_for_numbers("Hello fkin bitch")
